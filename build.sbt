name := "sqljedi"

organization := "com.lognex"

scalaVersion := "2.12.2"

javacOptions ++= Seq("-source", "1.8", "-target", "1.8")

scalacOptions ++= Seq(
  "-target:jvm-1.8",
  "-deprecation",
  "-encoding", "UTF-8",
  "-feature",
  "-unchecked",
  "-Xlint",
  "-Yno-adapted-args",
  "-Ywarn-value-discard"
)

libraryDependencies ++= Seq(
  "com.google.guava" % "guava" % "21.0" % "test",
  "com.novocode" % "junit-interface" % "0.11" % "test"
)

val repoUrl = "https://repo.moysklad.ru/artifactory/"
publishTo in Global := {
  if (isSnapshot.value) {
    Some("Moysklad Libs Snapshot Repo" at repoUrl + "libs-snapshot-local")
  } else {
    Some("Moysklad Libs Release Repo" at repoUrl + "libs-release-local")
  }
}