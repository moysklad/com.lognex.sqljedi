package com.lognex.sqljedi.info

import com.lognex.sqljedi.Expr._
import com.lognex.sqljedi.JoinType.InnerJoinType
import com.lognex.sqljedi._
import com.lognex.sqljedi.info.VarType.Free
import org.junit.Assert.assertEquals
import org.junit.Test

class VarsExprTraverserTest extends Implicits {

  private val g = new Alias
  private val f = new Alias
  private val c = new Alias
  private val m = new Alias
  private val o = new Alias
  private val s = new Alias

  private def freeVariables(expr: Expr): Set[Alias] = {
    val vi = new VarsInfo
    new VarsExprTraverser(Free, vi).traverse(expr)
    vi.lookupVars(Free).keys.toSet
  }

  private def freeVariables(select: SelectImpl): Set[Alias] = {
    select.info.lookupFreeVars.keys.toSet
  }

  @Test def testFreeVarsExpr(): Unit = {
    val goodId = new Param("1")

    val expr1 = BoolLiteral(false)
    assertEquals(Set.empty, freeVariables(expr1))

    val expr2 = (Field(g, "id") eq Parameter(goodId)) and false
    assertEquals(Set(g), freeVariables(expr2))

    val expr3 = DSL.coalesce(Field(f, "code"), Field(g, "code"))
    assertEquals(Set(g, f), freeVariables(expr3))
  }

  @Test def testFreeVarsSelect(): Unit = {
    val selectWithoutFreeVars = SelectImpl(
      columns = IndexedSeq.empty,
      relation = Table("good"),
      relationAlias = g,
      joins = IndexedSeq.empty,
      where = Field(g, "dType") eq StringLiteral("Good"),
      groupBy = IndexedSeq.empty,
      orderBy = IndexedSeq(Field(g, "name").asSort.asInstanceOf[SortClauseImpl]),
      limit = None,
      offset = None
    )
    assertEquals(Set.empty, freeVariables(selectWithoutFreeVars))

    val selectWithFreeVars = SelectImpl(
      columns = IndexedSeq((Field(c, "name"), "cons_name")),
      relation = Table("feature"),
      relationAlias = f,
      joins = IndexedSeq.empty,
      where = Field(f, "parent_id") eq Field(g, "id"),
      groupBy = IndexedSeq(Field(o, "name")),
      orderBy = IndexedSeq(Field(m, "name").asSort.asInstanceOf[SortClauseImpl]),
      limit = None,
      offset = None
    )
    assertEquals(Set(c, g, m, o), freeVariables(selectWithFreeVars))

    val selectWithJoins = SelectImpl(
      columns = IndexedSeq.empty,
      relation = Table("feature"),
      relationAlias = f,
      joins = IndexedSeq(JoinImpl(InnerJoinType, Table("good"), g, Field(g, "id") eq Field(f, "parent_id"))),
      where = true,
      groupBy = IndexedSeq.empty,
      orderBy = IndexedSeq(Field(g, "name").asSort.asInstanceOf[SortClauseImpl]),
      limit = None,
      offset = None
    )
    assertEquals(Set.empty, freeVariables(selectWithJoins))

    val wrappedSelect = SelectImpl(
      columns = IndexedSeq.empty,
      relation = selectWithFreeVars,
      relationAlias = s,
      joins = IndexedSeq.empty,
      where = true,
      groupBy = IndexedSeq.empty,
      orderBy = IndexedSeq(Field(s, "name").asSort.asInstanceOf[SortClauseImpl]),
      limit = None,
      offset = None
    )
    assertEquals(Set(c, g, m, o), freeVariables(wrappedSelect))

    val selectWithFreeVarFromUnion = SelectImpl(
      columns = IndexedSeq.empty,
      relation = Table("good"),
      relationAlias = g,
      joins = IndexedSeq.empty,
      where = Exists(UnionImpl(IndexedSeq(
        SelectImpl(IndexedSeq((Field(s, "x"), "one")), Table("s"), s),
        SelectImpl(IndexedSeq((Field(f, "y"), "two")), Table("s"), s)
      ))),
      groupBy = IndexedSeq.empty,
      orderBy = IndexedSeq(Field(g, "name").asSort.asInstanceOf[SortClauseImpl]),
      limit = None,
      offset = None
    )
    assertEquals(Set(f), freeVariables(selectWithFreeVarFromUnion))
  }
}
