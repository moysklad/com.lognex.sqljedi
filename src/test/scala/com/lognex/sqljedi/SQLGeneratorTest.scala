package com.lognex.sqljedi

import com.lognex.sqljedi.Expr._
import com.lognex.sqljedi.JoinType.InnerJoinType
import org.junit.Assert.assertEquals
import org.junit.Test

class SQLGeneratorTest extends Implicits {

  private val g = new Alias
  private val f = new Alias
  private val s = new Alias
  private val ss = new Alias

  private val aliasNames = Map(
    g -> "g",
    f -> "f",
    s -> "s",
    ss -> "ss"
  )

  private val goodId = new Param("4d8db381-2d8b-4726-b825-c2c1bb38ba25")

  private val paramNames = Map(
    goodId -> "goodId"
  )

  private def gen(expr: Expr): String = {
    val gen = new SQLGenerator(aliasNames, paramNames, prettyPrint = false)
    gen.write(expr)
    gen.result()
  }

  private def gen(select: Select): String = {
    val gen = new SQLGenerator(aliasNames, paramNames, prettyPrint = false)
    gen.write(select)
    gen.result()
  }

  @Test def testExpressions(): Unit = {

    val expr1 = (Field(g, "id") eq Parameter(goodId)) and false
    assertEquals("((g.id = :goodId) AND false)", gen(expr1))

    val expr2 = false and false and false and false
    assertEquals("(false AND false AND false AND false)", gen(expr2))

    val expr3 = DSL.coalesce(DSL.nil, "abc")
    assertEquals("coalesce(NULL, 'abc')", gen(expr3))

    val expr4 = false and (true or "a") and false
    assertEquals("(false AND (true OR 'a') AND false)", gen(expr4))

    val expr5 = DSL.case_()
      .when(BoolLiteral(true) eq false, "a")
      .else_("b")
      .end
    assertEquals("(CASE WHEN (true = false) THEN 'a' ELSE 'b' END)", gen(expr5))

    val expr6 = false and SubSelect(SelectImpl(
      columns = IndexedSeq((Field(g, "archived"), "archived")),
      relation = Table("good"),
      relationAlias = g,
      limit = Some(1)
    ))
    assertEquals("(false AND (SELECT g.archived AS archived FROM good AS g LIMIT 1))", gen(expr6))

    val expr7 = Field(s, "name".withPrefix(g))
    assertEquals("s.g_name", gen(expr7))

    val expr8 = s.$("cnt").in(SubSelect(UnionImpl(IndexedSeq(
      SelectImpl(IndexedSeq((IntLiteral(1), "one")), Table("s"), s),
      SelectImpl(IndexedSeq((IntLiteral(2), "two")), Table("ss"), ss)
    ))))
    assertEquals("s.cnt IN (((SELECT 1 AS one FROM s AS s) UNION (SELECT 2 AS two FROM ss AS ss)))", gen(expr8))

    val expr9 = Exists(UnionImpl(IndexedSeq(
      SelectImpl(IndexedSeq((IntLiteral(1), "one")), Table("s"), s),
      SelectImpl(IndexedSeq((IntLiteral(2), "two")), Table("ss"), ss)
    )))
    assertEquals("EXISTS ((SELECT 1 AS one FROM s AS s) UNION (SELECT 2 AS two FROM ss AS ss))", gen(expr9))
  }

  @Test def testSelects(): Unit = {
    val select1 = SelectImpl(
      columns = IndexedSeq.empty,
      relation = Table("good"),
      relationAlias = g
    )
    assertEquals("(SELECT FROM good AS g)", gen(select1))

    val select2 = SelectImpl(
      columns = IndexedSeq((Field(g, "name"), "good_name")),
      relation = Table("good"),
      relationAlias = g,
      joins = IndexedSeq(JoinImpl(InnerJoinType, Table("feature"), f, Field(g, "id") eq Field(f, "parent_id")))
    )
    assertEquals("(SELECT g.name AS good_name FROM good AS g INNER JOIN feature AS f ON (g.id = f.parent_id))", gen(select2))

    val select3 = SelectImpl(
      columns = IndexedSeq.empty,
      relation = Table("good"),
      relationAlias = g,
      groupBy = IndexedSeq(Field(g, "account_id"), Field(g, "dType"))
    )
    assertEquals("(SELECT FROM good AS g GROUP BY g.account_id, g.dtype)", gen(select3))

    val select4 = SelectImpl(
      columns = IndexedSeq.empty,
      relation = Table("good"),
      relationAlias = g,
      orderBy = IndexedSeq(Field(g, "name").asSort.asInstanceOf[SortClauseImpl], Field(g, "id").asSort.asInstanceOf[SortClauseImpl])
    )
    assertEquals("(SELECT FROM good AS g ORDER BY g.name ASC NULLS LAST, g.id ASC NULLS LAST)", gen(select4))

    val select5 = SelectImpl(
      columns = IndexedSeq.empty,
      relation = select1,
      relationAlias = s,
      joins = IndexedSeq(JoinImpl(InnerJoinType, select2, ss, true))
    )
    assertEquals("(SELECT FROM " +
      "(SELECT FROM good AS g) AS s " +
      "INNER JOIN (SELECT g.name AS good_name FROM good AS g INNER JOIN feature AS f ON (g.id = f.parent_id)) AS ss ON true)", gen(select5))

    val select6 = SelectImpl(
      columns = IndexedSeq.empty,
      relation = Table("good"),
      relationAlias = g,
      orderBy = IndexedSeq(Field(g, "name").asSort.asInstanceOf[SortClauseImpl], Field(g, "id").asSort.asInstanceOf[SortClauseImpl]),
      limit = Some(100),
      offset = Some(200)
    )
    assertEquals("(SELECT FROM good AS g ORDER BY g.name ASC NULLS LAST, g.id ASC NULLS LAST LIMIT 100 OFFSET 200)", gen(select6))

    val select7 = SelectImpl(
      columns = IndexedSeq.empty,
      relation = Table("good"),
      relationAlias = g,
      orderBy = IndexedSeq(Field(g, "name").asSort.desc.nullsMin.asInstanceOf[SortClauseImpl])
    )
    assertEquals("(SELECT FROM good AS g ORDER BY g.name DESC NULLS LAST)", gen(select7))
  }

  @Test def testUnions(): Unit = {
    val union1 = UnionImpl(IndexedSeq(
      SelectImpl(
        columns = IndexedSeq((Field(g, "id"), "id")),
        relation = Table("good"),
        relationAlias = g
      ),
      SelectImpl(
        columns = IndexedSeq((Field(f, "id"), "id")),
        relation = Table("feature"),
        relationAlias = f
      )
    ), orderBy = IndexedSeq(
      UnionSortClauseImpl(Left(1), directionIsAsc = false, Nulls.NullsFirst),
      UnionSortClauseImpl(Right("id"), directionIsAsc = true, Nulls.NullsLast)
    ), Some(10), Some(20))
    assertEquals("((SELECT g.id AS id FROM good AS g) UNION (SELECT f.id AS id FROM feature AS f)"
      + " ORDER BY 1 DESC NULLS FIRST, id ASC NULLS LAST LIMIT 10 OFFSET 20)", gen(union1))

    val union2 = UnionImpl(IndexedSeq(
      SelectImpl(
        columns = IndexedSeq((Field(g, "id"), "id")),
        relation = Table("good"),
        relationAlias = g
      ),
      SelectImpl(
        columns = IndexedSeq((Field(f, "id"), "id")),
        relation = Table("feature"),
        relationAlias = f
      )
    ))
    assertEquals("((SELECT g.id AS id FROM good AS g) UNION (SELECT f.id AS id FROM feature AS f))", gen(union2))
  }

  @Test def testStringLiteralsEscaping(): Unit = {
    assertEquals("'''; DROP TABLE goods;'", gen(StringLiteral("'; DROP TABLE goods;")))
  }
}
