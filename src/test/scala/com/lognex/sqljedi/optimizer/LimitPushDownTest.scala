package com.lognex.sqljedi.optimizer

import com.lognex.sqljedi.Expr.{Field, IntLiteral, SubSelect}
import com.lognex.sqljedi.JoinType.{LeftJoinLateralType, LeftJoinManyToOneType}
import com.lognex.sqljedi._
import org.junit.Assert.assertEquals
import org.junit.Test

class LimitPushDownTest extends Implicits {

  private val g = new Alias
  private val def_f = new Alias
  private val def_c = new Alias
  private val p_prc = new Alias
  private val p_curr = new Alias
  private val uom = new Alias
  private val gi = new Alias
  private val r = new Alias
  private val rr = new Alias

  @Test def testLimitPushDown(): Unit = {

    val subSelect = SelectImpl(
      columns = IndexedSeq(
        (Field(rr, "good_id"), "good_id"),
        (DSL.sum(Field(rr, "reserve")), "reserve_sum")
      ),
      relation = Table("reserve"),
      relationAlias = rr,
      groupBy = IndexedSeq(Field(rr, "good_id"))
    )

    val correlatedSubSelect = SelectImpl(
      columns = IndexedSeq(
        (Field(gi, "img_id"), "img_id")
      ),
      relation = Table("good_image"),
      relationAlias = gi,
      where = (Field(gi, "good_id") eq Field(g, "id")) and (Field(gi, "position") eq IntLiteral(0)),
      limit = Some(1)
    )

    val select = SelectImpl(
      columns = IndexedSeq(
        (Field(r, "reserve_sum"), "reserve_sum"),
        (Field(g, "name"), "good_name"),
        (Field(def_f, "name"), "def_feature_name"),
        (Field(def_c, "name"), "def_cons_name"),
        (Field(p_prc, "value"), "price_value"),
        (Field(p_curr, "code"), "curr_code"),
        (Field(uom, "name"), "uom_name"),
        (SubSelect(correlatedSubSelect), "img_id")
      ),
      relation = Table("good"),
      relationAlias = g,
      joins = IndexedSeq(
        JoinImpl(LeftJoinManyToOneType, subSelect, r, Field(r, "good_id") eq Field(g, "id")),
        JoinImpl(LeftJoinManyToOneType, Table("feature"), def_f, Field(def_f, "parent_id") eq Field(g, "id")),
        JoinImpl(LeftJoinManyToOneType, Table("consignment"), def_c, (Field(def_c, "good_id") eq Field(g, "id")) and (Field(def_c, "feature_id") eq Field(def_f, "id"))),
        JoinImpl(LeftJoinManyToOneType, Table("price"), p_prc, Field(p_prc, "parent_id") eq Field(def_f, "id")),
        JoinImpl(LeftJoinManyToOneType, Table("currency"), p_curr, Field(p_curr, "id") eq Field(p_prc, "currency_id")),
        JoinImpl(LeftJoinManyToOneType, Table("uom"), uom, Field(uom, "id") eq Field(g, "uom_id"))
      ),
      where = Field(p_prc, "value") gt 0,
      groupBy = IndexedSeq.empty,
      orderBy = IndexedSeq(Field(g, "code").asSort.asInstanceOf[SortClauseImpl]),
      limit = Some(1),
      offset = Some(10)
    )

    val actualOptimized = Optimizer.optimize(select, OptimizerConfigImpl())
    val subSelectAlias = actualOptimized.relationAlias

    val expectedOptimized = SelectImpl(
      columns = IndexedSeq(
        (Field(r, "reserve_sum"), "reserve_sum"),
        (Field(subSelectAlias, "name".withPrefix(g)), "good_name"),
        (Field(subSelectAlias, "name".withPrefix(def_f)), "def_feature_name"),
        (Field(def_c, "name"), "def_cons_name"),
        (Field(subSelectAlias, "value".withPrefix(p_prc)), "price_value"),
        (Field(p_curr, "code"), "curr_code"),
        (Field(uom, "name"), "uom_name"),
        (SubSelect(correlatedSubSelect.copy(
          where = (Field(gi, "good_id") eq Field(subSelectAlias, "id".withPrefix(g))) and (Field(gi, "position") eq IntLiteral(0))
        )), "img_id")
      ),
      relation = SelectImpl(
        columns = IndexedSeq(
          (Field(def_f, "id"), "id".withPrefix(def_f)),
          (Field(def_f, "name"), "name".withPrefix(def_f)),
          (Field(g, "code"), "code".withPrefix(g)),
          (Field(g, "id"), "id".withPrefix(g)),
          (Field(g, "name"), "name".withPrefix(g)),
          (Field(g, "uom_id"), "uom_id".withPrefix(g)),
          (Field(p_prc, "currency_id"), "currency_id".withPrefix(p_prc)),
          (Field(p_prc, "value"), "value".withPrefix(p_prc))
        ).sortBy(_._2),
        relation = Table("good"),
        relationAlias = g,
        joins = IndexedSeq(
          JoinImpl(LeftJoinManyToOneType, Table("feature"), def_f, Field(def_f, "parent_id") eq Field(g, "id")),
          JoinImpl(LeftJoinManyToOneType, Table("price"), p_prc, Field(p_prc, "parent_id") eq Field(def_f, "id"))
        ),
        where = Field(p_prc, "value") gt 0,
        groupBy = IndexedSeq.empty,
        orderBy = IndexedSeq(Field(g, "code").asSort.asInstanceOf[SortClauseImpl]),
        limit = Some(1),
        offset = Some(10)
      ),
      relationAlias = subSelectAlias,
      joins = IndexedSeq(
        JoinImpl(LeftJoinLateralType, subSelect.copy(
          where = subSelect.where and Field(rr, "good_id") eq Field(subSelectAlias, "id".withPrefix(g))
        ), r, true),
        JoinImpl(LeftJoinManyToOneType, Table("consignment"), def_c, (Field(def_c, "good_id") eq Field(subSelectAlias, "id".withPrefix(g)))
          and (Field(def_c, "feature_id") eq Field(subSelectAlias, "id".withPrefix(def_f)))),
        JoinImpl(LeftJoinManyToOneType, Table("currency"), p_curr, Field(p_curr, "id") eq Field(subSelectAlias, "currency_id".withPrefix(p_prc))),
        JoinImpl(LeftJoinManyToOneType, Table("uom"), uom, Field(uom, "id") eq Field(subSelectAlias, "uom_id".withPrefix(g)))
      ),
      where = true,
      groupBy = IndexedSeq.empty,
      orderBy = IndexedSeq(Field(subSelectAlias, "code".withPrefix(g)).asSort.asInstanceOf[SortClauseImpl]),
      limit = None,
      offset = None
    )

    assertEquals(expectedOptimized, actualOptimized)
  }
}
