package com.lognex.sqljedi.optimizer

import com.lognex.sqljedi.Expr.{BoolLiteral, Field, Parameter}
import com.lognex.sqljedi.JoinType.LeftJoinManyToOneType
import com.lognex.sqljedi._
import org.junit.Assert.{assertEquals, assertFalse, assertTrue}
import org.junit.Test

class OptimizerTest extends Implicits {

  private val g = new Alias
  private val def_f = new Alias
  private val def_c = new Alias
  private val p_prc = new Alias
  private val p_curr = new Alias
  private val uom = new Alias


  private val Joins = IndexedSeq(
    JoinImpl(LeftJoinManyToOneType, Table("feature"), def_f, Field(def_f, "parent_id") eq Field(g, "id")),
    JoinImpl(LeftJoinManyToOneType, Table("consignment"), def_c, (Field(def_c, "good_id") eq Field(g, "id")) and (Field(def_c, "feature_id") eq Field(def_f, "id"))),
    JoinImpl(LeftJoinManyToOneType, Table("price"), p_prc, Field(p_prc, "parent_id") eq Field(def_f, "id")),
    JoinImpl(LeftJoinManyToOneType, Table("currency"), p_curr, Field(p_curr, "id") eq Field(p_prc, "currency_id")),
    JoinImpl(LeftJoinManyToOneType, Table("uom"), uom, Field(uom, "id") eq Field(g, "uom_id"))
  )

  @Test def testJoinUsages(): Unit = {
    val select = SelectImpl(
      columns = IndexedSeq.empty,
      relation = Table("good"),
      relationAlias = g,
      joins = Joins,
      where = true,
      groupBy = IndexedSeq.empty,
      orderBy = IndexedSeq.empty,
      limit = None,
      offset = None
    )

    def usages(alias: Alias) = select.info.joinUsages(alias).map(_.join.relationAlias).toSet

    assertEquals(Set(def_c, p_prc), usages(def_f))
    assertEquals(Set(), usages(def_c))
    assertEquals(Set(p_curr), usages(p_prc))
    assertEquals(Set(), usages(p_curr))
    assertEquals(Set(), usages(uom))
  }

  @Test def testDoesNotAffectResultSet(): Unit = {
    val select = SelectImpl(
      columns = IndexedSeq.empty,
      relation = Table("good"),
      relationAlias = g,
      joins = Joins,
      where = true,
      groupBy = IndexedSeq.empty,
      orderBy = IndexedSeq.empty,
      limit = None,
      offset = None
    )

    def affects(alias: Alias) = select.info.lookupJoinInfos(alias).doesAffectResultSet

    assertFalse(affects(def_f))
    assertFalse(affects(def_c))
    assertFalse(affects(p_prc))
    assertFalse(affects(p_curr))
    assertFalse(affects(uom))
  }

  @Test def testAffectsResultSet(): Unit = {
    val currName = new Param("USD")

    val select = SelectImpl(
      columns = IndexedSeq.empty,
      relation = Table("good"),
      relationAlias = g,
      joins = Joins,
      where = BoolLiteral(true) or (Field(p_curr, "name") eq Parameter(currName)),
      groupBy = IndexedSeq.empty,
      orderBy = IndexedSeq.empty,
      limit = None,
      offset = None
    )

    def affects(alias: Alias) = select.info.lookupJoinInfos(alias).doesAffectResultSet

    assertTrue (affects(def_f))
    assertFalse(affects(def_c))
    assertTrue (affects(p_prc))
    assertTrue (affects(p_curr))
    assertFalse(affects(uom))
  }

  @Test def testUsedInProjections(): Unit = {
    val select = SelectImpl(
      columns = IndexedSeq((Field(p_prc, "name"), "price_name")),
      relation = Table("good"),
      relationAlias = g,
      joins = Joins,
      where = true,
      groupBy = IndexedSeq.empty,
      orderBy = IndexedSeq.empty,
      limit = None,
      offset = None
    )

    def used(alias: Alias) = select.info.lookupJoinInfos(alias).isUsedInProjections

    assertTrue (used(def_f))
    assertFalse(used(def_c))
    assertTrue (used(p_prc))
    assertFalse(used(p_curr))
    assertFalse(used(uom))
  }

  @Test def testRemoveUselessJoins(): Unit = {
    val select = SelectImpl(
      columns = IndexedSeq((Field(p_prc, "name"), "price_name")),
      relation = Table("good"),
      relationAlias = g,
      joins = Joins,
      where = true,
      groupBy = IndexedSeq.empty,
      orderBy = IndexedSeq.empty,
      limit = None,
      offset = None
    )

    val optimized = Optimizer.optimize(select, OptimizerConfigImpl())
    assertEquals(Seq(def_f, p_prc), optimized.joins.map(_.relationAlias))
  }
}
