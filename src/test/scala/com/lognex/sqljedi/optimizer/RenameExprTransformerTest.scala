package com.lognex.sqljedi.optimizer

import com.lognex.sqljedi.Expr.{BoolLiteral, Field, Parameter, SubSelect}
import com.lognex.sqljedi._
import org.junit.Assert.assertEquals
import org.junit.Test

class RenameExprTransformerTest extends Implicits {

  private val a = new Alias
  private val g = new Alias
  private val f = new Alias
  private val c = new Alias
  private val m = new Alias
  private val o = new Alias
  private val zzz = new Alias

  private val Renames = Map(
    Field(a, "name") -> Field(zzz, "a_name"),
    Field(o, "name") -> Field(zzz, "o_name")
  )

  private def rename(expr: Expr): Expr = new FieldSubstitutionExprTransformer {
    override def substitute(field: Field): Expr = Renames.getOrElse(field, field)
  }.transform(expr)

  @Test def testRenameExpr(): Unit = {
    val goodId = new Param("1")

    val expr1 = BoolLiteral(false)
    assertEquals(expr1, rename(expr1))

    val expr2 = (Field(a, "name") eq Parameter(goodId)) and false
    assertEquals((Field(zzz, "a_name") eq Parameter(goodId)) and false, rename(expr2))
  }

  @Test def testRenameSubSelect(): Unit = {
    val select = SelectImpl(
      columns = IndexedSeq.empty,
      relation = SelectImpl(
        columns = IndexedSeq((Field(c, "name"), "cons_name")),
        relation = Table("feature"),
        relationAlias = f,
        joins = IndexedSeq.empty,
        where = Field(f, "parent_id") eq Field(g, "id"),
        groupBy = IndexedSeq(Field(o, "name")),
        orderBy = IndexedSeq(Field(m, "name").asSort.asInstanceOf[SortClauseImpl]),
        limit = None,
        offset = None
      ),
      relationAlias = a,
      joins = IndexedSeq.empty,
      where = true,
      groupBy = IndexedSeq.empty,
      orderBy = IndexedSeq(Field(a, "name").asSort.asInstanceOf[SortClauseImpl]),
      limit = None,
      offset = None
    )

    val renamed = SelectImpl(
      columns = IndexedSeq.empty,
      relation = SelectImpl(
        columns = IndexedSeq((Field(c, "name"), "cons_name")),
        relation = Table("feature"),
        relationAlias = f,
        joins = IndexedSeq.empty,
        where = Field(f, "parent_id") eq Field(g, "id"),
        groupBy = IndexedSeq(Field(zzz, "o_name")),
        orderBy = IndexedSeq(Field(m, "name").asSort.asInstanceOf[SortClauseImpl]),
        limit = None,
        offset = None
      ),
      relationAlias = a,
      joins = IndexedSeq.empty,
      where = true,
      groupBy = IndexedSeq.empty,
      orderBy = IndexedSeq(Field(zzz, "a_name").asSort.asInstanceOf[SortClauseImpl]),
      limit = None,
      offset = None
    )

    assertEquals(SubSelect(renamed), rename(SubSelect(select)))
  }
}
