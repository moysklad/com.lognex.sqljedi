package com.lognex.sqljedi

import com.lognex.sqljedi.Expr.{BoolLiteral, IntLiteral, StringLiteral}

import scala.language.implicitConversions

private[sqljedi] trait Implicits {

  implicit def str2name(s: String): Name = Name(s)

  implicit def bool2expr(b: Boolean): Expr = BoolLiteral(b)

  implicit def str2expr(s: String): Expr = StringLiteral(s)

  implicit def int2expr(i: Int): Expr = IntLiteral(i)
}
