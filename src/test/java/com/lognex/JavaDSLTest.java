package com.lognex;

import com.lognex.sqljedi.*;
import org.junit.Test;

import static com.google.common.collect.ImmutableMap.of;
import static com.lognex.sqljedi.DSL.*;

public class JavaDSLTest {

    private final Alias g = newAlias(), f = newAlias(), s = newAlias(), u = newAlias();
    private final Alias c0 = newAlias(), c1 = newAlias(), c2 = newAlias();

    @Test
    public void testSelect() {
        Join joinFeature = innerJoin("feature", f, f.$("parent_id").eq(g.$("id")));
        System.out.println(newContext(of(g, "g", f, "f")).toSql(joinFeature));

        Select select = select(of(
                "good_name", g.$("name"),
                "good_code", g.$("code"),
                "good_weight", g.$("weight")
        ))
                .from("good", g)
                .innerJoin("feature", f, f.$("parent_id").eq(g.$("id")))
                .where(g.$("name").eq(str("XBox"))
                        .and(f.$("code").eq(str("0001")))
                        .and(num(1).eq(select(of("count", count(num(1))))
                                .from("userinfo", u)
                                .where(u.$("name").eq(str("admin@demo")))
                                .buildExpr()))
                )
                .groupBy(g.$("id"))
                .orderBy(g.$("name").asSort().nullsMin())
                .limit(1)
                .offset(1)
                .build();

        Select count = select(of(
                "count", count(num(1)),
                "sumWeight", sum(s.$("good_weight"))
        )).from(select, s).build();

        System.out.println(newContext().toSql(count.optimize()));
    }

    @Test
    public void testExpressions() {
        Expr ex1 = g.$("name");

        Expr ex2 = g.$("name").eq(str("XBox"))
                .and((g.$("weight").gt(num(1000))).and(g.$("code").isNotNull()));

        Expr ex3 = coalesce(f.$("code"), g.$("code"), str(""));

        Expr quantity = coalesce(c0.$("stock"), num(0)).minus(coalesce(c1.$("reserve"), num(0))).plus(coalesce(c2.$("reserve"), num(0)));

        Expr caseEx = case_()
                .when(g.$("dtype").eq(str("Good")), str("This is Good"))
                .when(g.$("dtype").eq(str("Service")), str("This is Service"))
                .else_(nil())
                .end();

        Expr subSelect = select(of("g_name", s.$("name")))
                .from(select(of("name", g.$("name")))
                        .from("good", g)
                        .where(g.$("code").eq(u.$("uid")))
                        .build(), s)
                .where(s.$("parent_id").eq(f.$("id"))).buildExpr();

        Expr exists = exists(select(of("1", num(1))).from("good", g).build());

        System.out.println(newContext(of(g, "g")).toSql(ex1));
        System.out.println();
        System.out.println(newContext(of(g, "g")).toSql(ex2));
        System.out.println();
        System.out.println(newContext(of(g, "g", f, "f")).toSql(ex3));
        System.out.println();
        System.out.println(newContext(of(c0, "c0", c1, "c1", c2, "c2")).toSql(quantity));
        System.out.println();
        System.out.println(newContext(of(g, "g")).toSql(caseEx));
        System.out.println();
        System.out.println(newContext(of(f, "f", u, "u")).toSql(subSelect));
        System.out.println();
        System.out.println(newContext(of(g, "g")).toSql(exists));
    }

    @Test
    public void testJoins() {
        Join join = innerJoin(select(of("name", g.$("name")))
                .from("good", g)
                .where(g.$("code").eq(u.$("uid")))
                .build(), s, s.$("name").eq(u.$("uid")));

        System.out.println(newContext(of(u, "u")).toSql(join));
    }

    @Test
    public void testSQLContext() {
        Alias g = newAlias(), f = newAlias();

        Expr name = g.$("name");

        Join join = innerJoin("good", g, g.$("id").eq(f.$("parent_id")));

        SQLContext ctx = newContext(of(f, "f"));

        String legacySql = "select " + name.toSql(ctx) + " from feature f " + join.toSql(ctx);
        System.out.println(legacySql);
    }

    @Test
    public void testTotal() {
        Select select = select(of("buyPrice", g.$("buyPrice")))
                .from("good", g)
                .where(g.$("code").eq(str("123")))
                .orderBy(g.$("name").asSort())
                .limit(100)
                .offset(200)
                .build();

        Select totalSelect = select.total(of("buYprice", (expr -> coalesce(sum(expr), num(0)))));

        System.out.println(newContext().toSql(totalSelect));
    }
}
