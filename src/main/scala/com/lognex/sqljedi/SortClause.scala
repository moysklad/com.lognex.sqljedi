package com.lognex.sqljedi

private sealed trait GenSortClause[E] {
  def expr: E
  def directionIsAsc: Boolean
  def nulls: Nulls
}

sealed trait SortClause {

  final def direction(asc: Boolean): SortClause = this match {
    case c: SortClauseImpl => c.copy(directionIsAsc = asc)
  }

  final def asc: SortClause = direction(asc = true)

  final def desc: SortClause = direction(asc = false)

  private def nulls(nulls: Nulls): SortClause = this match {
    case c: SortClauseImpl => c.copy(nulls = nulls)
  }

  final def nullsLast: SortClause = nulls(nulls = Nulls.NullsLast)

  final def nullsFirst: SortClause = nulls(nulls = Nulls.NullsFirst)

  final def nullsMax: SortClause = nulls(nulls = Nulls.NullsMax)

  final def nullsMin: SortClause = nulls(nulls = Nulls.NullsMin)
}

private final case class SortClauseImpl(expr: Expr,
                                        directionIsAsc: Boolean,
                                        nulls: Nulls) extends SortClause with GenSortClause[Expr]

sealed trait UnionSortClause {

  final def direction(asc: Boolean): UnionSortClause = this match {
    case c: UnionSortClauseImpl => c.copy(directionIsAsc = asc)
  }

  final def asc: UnionSortClause = direction(asc = true)

  final def desc: UnionSortClause = direction(asc = false)

  private def nulls(nulls: Nulls): UnionSortClause = this match {
    case c: UnionSortClauseImpl => c.copy(nulls = nulls)
  }

  final def nullsLast: UnionSortClause = nulls(nulls = Nulls.NullsLast)

  final def nullsFirst: UnionSortClause = nulls(nulls = Nulls.NullsFirst)

  final def nullsMax: UnionSortClause = nulls(nulls = Nulls.NullsMax)

  final def nullsMin: UnionSortClause = nulls(nulls = Nulls.NullsMin)
}

private final case class UnionSortClauseImpl(expr: Either[Int, Name],
                                        directionIsAsc: Boolean,
                                        nulls: Nulls) extends UnionSortClause with GenSortClause[Either[Int, Name]]

private sealed trait Nulls

private object Nulls {
  case object NullsLast extends Nulls
  case object NullsFirst extends Nulls
  case object NullsMax extends Nulls
  case object NullsMin extends Nulls
}