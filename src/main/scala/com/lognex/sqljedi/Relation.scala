package com.lognex.sqljedi

import com.lognex.sqljedi.Expr.{BoolLiteral, SubSelect}
import com.lognex.sqljedi.info.SelectInfo
import com.lognex.sqljedi.optimizer.Optimizer

import scala.collection.JavaConverters._

sealed trait Relation

private final case class Table(name: Name) extends Relation

sealed trait Select extends Relation {

  final def asExpr: Expr = this match {
    case s: Select => SubSelect(s)
  }

  final def optimize(): Select = optimize(OptimizerConfigImpl())

  final def optimize(config: OptimizerConfig): Select = this match {
    case s: SelectImpl => Optimizer.optimize(s, config match {case c: OptimizerConfigImpl => c})
    case s: UnionImpl => s
  }

  final def total(aggregates: java.util.Map[String, java.util.function.Function[Expr, Expr]]): Select = this match {
    case s: SelectImpl =>
      require(s.groupBy.isEmpty)
      val aggColumns = IndexedSeq.newBuilder[(Expr, Name)]

      for ((name, func) <- aggregates.asScala) {
        s.columns.find(_._2 == Name(name)) match {
          case Some((col, colName)) => aggColumns += ((func.apply(col), colName))
          case None =>
        }
      }

      s.copy(
        columns = aggColumns.result(),
        orderBy = IndexedSeq.empty,
        limit = None,
        offset = None
      )
    case _: UnionImpl => throw new UnsupportedOperationException
  }

  final def toSql(context: SQLContext): String = context.toSql(this)
}

private final case class SelectImpl(columns: IndexedSeq[(Expr, Name)],
                                    relation: Relation,
                                    relationAlias: Alias,
                                    joins: IndexedSeq[JoinImpl] = IndexedSeq.empty,
                                    where: Expr = BoolLiteral(true),
                                    groupBy: IndexedSeq[Expr] = IndexedSeq.empty,
                                    orderBy: IndexedSeq[SortClauseImpl] = IndexedSeq.empty,
                                    limit: Option[Int] = None,
                                    offset: Option[Int] = None) extends Select {
  val info = new SelectInfo(this)
}

private final case class UnionImpl(selects: IndexedSeq[Select],
                                   orderBy: IndexedSeq[UnionSortClauseImpl] = IndexedSeq.empty,
                                   limit: Option[Int] = None,
                                   offset: Option[Int] = None) extends Select {
  require(selects.lengthCompare(2) >= 0)
}