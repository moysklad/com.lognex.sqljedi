package com.lognex.sqljedi

sealed trait OptimizerConfig {
  def limitThreshold(limitThreshold: Int): OptimizerConfig
}

private [sqljedi] final case class OptimizerConfigImpl(limitThreshold: Int = 200) extends OptimizerConfig {
  override def limitThreshold(limitThreshold: Int): OptimizerConfig = copy(limitThreshold = limitThreshold)
}
