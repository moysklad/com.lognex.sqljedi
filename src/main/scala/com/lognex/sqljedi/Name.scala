package com.lognex.sqljedi

private final class Name private (val underlying: String, val prefix: Option[Alias]) extends Ordered[Name] {

  override def compare(that: Name): Int = this.toString.compare(that.toString)

  def withPrefix(prefix: Alias): Name = new Name(this.underlying, Some(prefix))


  override def equals(other: Any): Boolean = other match {
    case that: Name => underlying == that.underlying && prefix == that.prefix
    case _ => false
  }

  override def hashCode(): Int = underlying.hashCode

  override def toString: String = prefix match {
    case Some(p) => p + "_" + underlying
    case None => underlying
  }
}

private object Name {

  private val ValidSQLName = "^\\w+$".r // just to prevent injection

  def apply(name: String): Name = {
    require(ValidSQLName.pattern.matcher(name).matches())
    new Name(name.toLowerCase, None)
  }
}
