package com.lognex.sqljedi

import java.util.regex.Pattern

import com.lognex.sqljedi.Expr._
import com.lognex.sqljedi.JoinType.{InnerJoinType, LeftJoinLateralType, LeftJoinManyToOneType, LeftJoinType}

private final class SQLGenerator(aliasToSql: Alias => String,
                                 paramToSql: Param => String,
                                 prettyPrint: Boolean) {

  private val SingleQuotePattern: Pattern = Pattern.compile("'")

  private val builder = new Builder()
  private var indentLevel = 0

  private final class Builder {
    private val sb = new StringBuilder

    def append(char: Char): Builder = {sb.append(char); this}
    def append(string: String): Builder = {sb.append(string); this}
    def append(alias: Alias): Builder = {sb.append(aliasToSql(alias)); this}
    def append(param: Param): Builder = {sb.append(paramToSql(param)); this}

    def append(name: Name): Builder = {
      for (prefix <- name.prefix) {
        append(prefix).append('_')
      }
      sb.append(name.underlying)
      this
    }

    def result(): String = sb.result()
  }

  def result(): String = builder.result()

  private def nl(): Unit = {
    if (prettyPrint) {
      builder append '\n'
      for (_ <- 0 until indentLevel) {
        builder append "  "
      }
    } else {
      builder append ' '
    }
    ()
  }

  def write(relation: Relation, commentTag: String = ""): Unit = {
    if (commentTag.nonEmpty) {
        builder append "/*" append commentTag append "*/"
    }

    relation match {

      case Table(name) => builder append name

      case SelectImpl(columns, rel, relAlias, joins, where, groupBy, orderBy, limit, offset) =>
        indentLevel += 1
        builder append "(SELECT"

        writeColumns(columns)

        nl()
        builder append "FROM "
        write(rel)
        builder append " AS " append relAlias

        for (join <- joins) {
          nl()
          write(join)
        }

        if (where != BoolLiteral(true)) {
          nl()
          builder append "WHERE "
          write(where)
        }

        if (groupBy.nonEmpty) {
          nl()
          builder append "GROUP BY "
          writeSeparated(groupBy, ", ", newline = false, brackets = false)
        }
        writeEndingClauses(orderBy, limit, offset, (expr: Expr) => write(expr))

        indentLevel -= 1
        builder append ')'

      case UnionImpl(selects, orderBy, limit, offset) =>
        indentLevel += 1
        builder append "("

        var first = true
        for (select <- selects) {
          if (!first) {
            nl()
            builder append "UNION"
            nl()
          }
          write(select)
          first = false
        }

        writeEndingClauses(orderBy, limit, offset, (expr: Either[Int, Name]) => expr match {
          case Left(num) => builder append num.toString; ()
          case Right(name) => builder append name; ()
        })

        indentLevel -= 1
        builder append ")"
    }
    ()
  }

  private def writeEndingClauses[E](orderBy: IndexedSeq[GenSortClause[E]], limit: Option[Int], offset: Option[Int], writeSortExpr: (E) => Unit): Unit = {
    if (orderBy.nonEmpty) {
      nl()
      builder append "ORDER BY "
      var first = true
      for (ord <- orderBy) {
        if (!first) {
          builder append ", "
        }
        writeSortExpr(ord.expr)
        builder append (if (ord.directionIsAsc) " ASC" else " DESC")
        builder append ((ord.directionIsAsc, ord.nulls) match {
          case (true, Nulls.NullsMax) | (false, Nulls.NullsMin) | (_, Nulls.NullsLast) => " NULLS LAST"
          case (true, Nulls.NullsMin) | (false, Nulls.NullsMax) | (_, Nulls.NullsFirst) => " NULLS FIRST"
        })
        first = false
      }
    }

    for (limitValue <- limit) {
      nl()
      builder append "LIMIT " append limitValue.toString
    }
    for (offsetValue <- offset) {
      nl()
      builder append "OFFSET " append offsetValue.toString
    }
  }

  private def writeColumns(columns: IndexedSeq[(Expr, Name)]): Unit = {
    indentLevel += 1
    var first = true
    for ((ex, alias) <- columns) {
      nl()
      if (!first) {
        builder append ", "
      }
      write(ex)
      builder append " AS " append alias
      first = false
    }
    indentLevel -= 1
  }

  def write(join: JoinImpl): Unit = join match {
    case JoinImpl(tpe, relation, relationAlias, condition) =>
      val typeStr = tpe match {
        case InnerJoinType => "INNER JOIN"
        case LeftJoinType | LeftJoinManyToOneType => "LEFT JOIN"
        case LeftJoinLateralType => "LEFT JOIN LATERAL"
      }
      builder append typeStr append ' '
      write(relation)
      builder append " AS " append relationAlias append " ON "
      write(condition)
  }

  def write(expr: Expr): Unit = {
    expr match {

      case Parameter(name) => builder append ':' append name

      case SubSelect(select) => write(select)

      case Exists(select) =>
        builder append "EXISTS "
        write(select)

      case Field(relationAlias, name) => builder append relationAlias append '.' append name

      case Null => builder append "NULL"

      case IsNull(ex) =>
        write(ex)
        builder append " IS NULL"

      case IsNotNull(ex) =>
        write(ex)
        builder append " IS NOT NULL"

      case BoolLiteral(value) => builder append value.toString

      case StringLiteral(value) => builder append ''' append SingleQuotePattern.matcher(value).replaceAll("''") append '''

      case IntLiteral(value) => builder append value.toString

      case And(exs) => writeSeparated(exs, "AND ", newline = true, brackets = true)

      case Or(exs) => writeSeparated(exs, "OR ", newline = true, brackets = true)

      case In(ex, values) =>
        write(ex)
        builder append " IN "
        writeSeparated(values, ",", newline = false, brackets = true)

      case Operator(literal, left, right) =>
        builder append '('
        write(left)
        builder append ' ' append literal append ' '
        write(right)
        builder append ')'

      case Function(name, arguments) =>
        builder append name
        writeSeparated(arguments, ", ", newline = false, brackets = true)

      case Case(cases, defaultCase) =>
        indentLevel += 1
        builder append "(CASE"
        for ((condition, ex) <- cases) {
          nl()
          builder append "WHEN "
          write(condition)
          builder append " THEN "
          write(ex)
        }
        for (dc <- defaultCase) {
          nl()
          builder append "ELSE "
          write(dc)
        }
        indentLevel -= 1
        nl()
        builder append "END)"

      case Cast(ex, typeLiteral) =>
        builder append "CAST("
        write(ex)
        builder append " AS " append typeLiteral append ")"
    }
    ()
  }

  private def writeSeparated(exs: Traversable[Expr], separator: String, newline: Boolean, brackets: Boolean): Any = {
    if (newline) {
      indentLevel += 1
    }
    if (brackets) {
      builder append '('
    }
    var first = true
    for (ex <- exs) {
      if (!first) {
        if (newline) {
          nl()
        }
        builder append separator
      }
      write(ex)
      first = false
    }
    if (newline) {
      indentLevel -= 1
    }
    if (brackets) {
      builder append ')'
    }
  }
}
