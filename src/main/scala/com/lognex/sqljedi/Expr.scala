package com.lognex.sqljedi

import com.lognex.sqljedi.Expr._

import scala.annotation.varargs
import scala.collection.JavaConverters._

sealed trait Expr {

  final def and(that: Expr): Expr = (this, that) match {

    case (a, BoolLiteral(true)) => a
    case (BoolLiteral(true), b) => b

    case (a: And, b: And) => And(a.exprs ++ b.exprs)
    case (a: And, b     ) => And(a.exprs :+ b      )
    case (a     , b: And) => And(a       +: b.exprs)

    case (a, b) => And(IndexedSeq(a, b))
  }

  final def or(that: Expr): Expr = (this, that) match {

    case (a, BoolLiteral(false)) => a
    case (BoolLiteral(false), b) => b

    case (a: Or, b: Or) => Or(a.exprs ++ b.exprs)
    case (a: Or, b    ) => Or(a.exprs :+ b      )
    case (a    , b: Or) => Or(a       +: b.exprs)

    case (a, b) => Or(IndexedSeq(a, b))
  }

  final def eq(that: Expr): Expr = Operator("=", this, that)

  final def neq(that: Expr): Expr = Operator("<>", this, that)

  final def isDistinctFrom(that: Expr): Expr = Operator("IS DISTINCT FROM", this, that)

  final def isNotDistinctFrom(that: Expr): Expr = Operator("IS NOT DISTINCT FROM", this, that)

  final def gt(that: Expr): Expr = Operator(">", this, that)

  final def gte(that: Expr): Expr = Operator(">=", this, that)

  final def lt(that: Expr): Expr = Operator("<", this, that)

  final def lte(that: Expr): Expr = Operator("<=", this, that)

  final def plus(that: Expr): Expr = Operator("+", this, that)

  final def minus(that: Expr): Expr = Operator("-", this, that)

  final def mult(that: Expr): Expr = Operator("*", this, that)

  final def div(that: Expr): Expr = Operator("/", this, that)

  final def concat(that: Expr): Expr = Operator("||", this, that)

  final def jsonGet(that: Expr): Expr = Operator("->", this, that)

  final def jsonGetText(that: Expr): Expr = Operator("->>", this, that)

  final def operator(literal: String, that: Expr): Expr = Operator(literal, this, that)

  final def isNull: Expr = IsNull(this)

  final def isNotNull: Expr = IsNotNull(this)

  @varargs
  final def in(exprs: Expr*): Expr = In(this, IndexedSeq(exprs: _*))

  final def in(expr: java.lang.Iterable[Expr]): Expr = In(this, expr.asScala.toIndexedSeq)

  final def like(that: Expr): Expr = Operator("like", this, that)

  final def iLike(that: Expr): Expr = Operator("iLike", this, that)

  final def tsMatch(that: Expr): Expr = Operator("@@", this, that)

  final def castTo(tpe: String): Expr = Cast(this, tpe)

  final def asSort: SortClause = SortClauseImpl(this, directionIsAsc = true, nulls = Nulls.NullsMax) // Null is max value by default

  final def toSql(context: SQLContext): String = context.toSql(this)
}

private object Expr {

  final case class Parameter(param: Param) extends Expr

  final case class SubSelect(select: Select) extends Expr

  final case class Field(relationAlias: Alias, name: Name) extends Expr

  case object Null extends Expr

  final case class IsNull(expr: Expr) extends Expr

  final case class IsNotNull(expr: Expr) extends Expr

  final case class BoolLiteral(value: Boolean) extends Expr

  final case class StringLiteral(value: String) extends Expr

  final case class IntLiteral(value: Int) extends Expr

  final case class And(exprs: IndexedSeq[Expr]) extends Expr {
    require(exprs.length >= 2)
  }

  final case class Or(exprs: IndexedSeq[Expr]) extends Expr {
    require(exprs.length >= 2)
  }

  final case class In(expr: Expr, values: IndexedSeq[Expr]) extends Expr {
    require(values.nonEmpty)
  }

  final case class Exists(select: Select) extends Expr

  final case class Operator(literal: String, left: Expr, right: Expr) extends Expr

  final case class Function(name: String, arguments: IndexedSeq[Expr]) extends Expr

  final case class Case(cases: IndexedSeq[(Expr, Expr)], defaultCase: Option[Expr]) extends Expr {
    require(cases.nonEmpty)
  }

  final case class Cast(expr: Expr, typeLiteral: String) extends Expr
}
