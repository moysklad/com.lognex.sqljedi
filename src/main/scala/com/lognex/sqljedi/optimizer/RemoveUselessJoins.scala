package com.lognex.sqljedi.optimizer
import com.lognex.sqljedi.{OptimizerConfigImpl, SelectImpl}

private final class RemoveUselessJoins extends Optimization {

  override def transform(select: SelectImpl, config: OptimizerConfigImpl): SelectImpl = {

    val uselessJoins = select.info.lookupJoinInfos.valuesIterator
      .filter(j => !j.isUsedInProjections && !j.doesAffectResultSet)
      .map(_.alias)
      .toSet

    if (uselessJoins.nonEmpty) {
      select.copy(joins = select.joins.filter(j => !uselessJoins.contains(j.relationAlias)))
    } else {
      select
    }
  }
}
