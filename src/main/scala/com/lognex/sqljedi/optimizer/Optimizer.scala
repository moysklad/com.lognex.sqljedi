package com.lognex.sqljedi.optimizer

import com.lognex.sqljedi._

private[sqljedi] object Optimizer {

  def optimize(select: SelectImpl, config: OptimizerConfigImpl): SelectImpl = {
      var result = select
      for (opt <- Optimizer.Optimizations) {
        result = opt.transform(result, config)
      }
      result
  }

  private val Optimizations: IndexedSeq[Optimization] = IndexedSeq(
    new RemoveUselessJoins,
    new LimitPushDown
  )
}