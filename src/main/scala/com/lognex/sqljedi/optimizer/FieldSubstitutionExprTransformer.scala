package com.lognex.sqljedi.optimizer

import com.lognex.sqljedi.Expr._
import com.lognex.sqljedi._

private abstract class FieldSubstitutionExprTransformer extends ExprTransformer {

  def substitute(field: Field): Expr

  override def transform(expr: Expr): Expr = expr match {

    case SubSelect(select) => SubSelect(transform(select))

    case Exists(select) => Exists(transform(select))

    case fld: Field => substitute(fld)

    case ex => super.transform(ex)
  }

  private def transform(relation: Relation): Relation = relation match {
    case select: Select => transform(select)
    case r => r
  }

  private def transform(select: Select): Select = select match {
    case selectImpl: SelectImpl =>
      val columns = selectImpl.columns map {
        case (ex, name) => (transform(ex), name)
      }

      val relation = transform(selectImpl.relation)

      val joins = selectImpl.joins map {
        case JoinImpl(tpe, r, ra, condition) => JoinImpl(tpe, transform(r), ra, transform(condition))
      }

      val where = transform(selectImpl.where)
      val groupBy = selectImpl.groupBy map transform
      val orderBy = selectImpl.orderBy map (ord => ord.copy(expr = transform(ord.expr)))

      selectImpl.copy(
        columns = columns,
        relation = relation,
        joins = joins,
        where = where,
        groupBy = groupBy,
        orderBy = orderBy
      )
    case unionImpl: UnionImpl =>
      unionImpl.copy(
        selects = unionImpl.selects map transform
      )
  }
}
