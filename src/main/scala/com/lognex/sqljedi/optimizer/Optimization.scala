package com.lognex.sqljedi.optimizer

import com.lognex.sqljedi.{OptimizerConfigImpl, SelectImpl}

private trait Optimization {
  def transform(select: SelectImpl, config: OptimizerConfigImpl): SelectImpl
}
