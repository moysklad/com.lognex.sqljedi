package com.lognex.sqljedi.optimizer

import com.lognex.sqljedi.Expr.{BoolLiteral, Field}
import com.lognex.sqljedi.JoinType.{LeftJoinLateralType, LeftJoinManyToOneType}
import com.lognex.sqljedi.{Alias, Expr, OptimizerConfigImpl, SelectImpl}

import scala.collection.mutable

private final class LimitPushDown extends Optimization {

  override def transform(select: SelectImpl, config: OptimizerConfigImpl): SelectImpl = {

    val noGroupBy = select.groupBy.isEmpty

    val hasOrderBy = select.orderBy.nonEmpty

    val hasSelectiveLimit = select.limit match {
      case Some(l) => l <= config.limitThreshold
      case None => false
    }

    val joinsToMoveDown = select.info.lookupJoinInfos.valuesIterator
      .filter(j => !j.doesAffectResultSet)
      .map(_.alias)
      .toSet

    if (noGroupBy && hasOrderBy && hasSelectiveLimit && joinsToMoveDown.nonEmpty) {
      pushDownLimit(select, joinsToMoveDown)
    } else {
      select
    }
  }

  private def pushDownLimit(select: SelectImpl, joinsToMoveDown: Set[Alias]): SelectImpl = {

    val subSelectAlias = new Alias
    val si = select.info

    val vars = new mutable.HashMap[Alias, mutable.Set[Field]]() with mutable.MultiMap[Alias, Field]
    for (fv <- si.lookupProjectionVars; fld <- fv._2) {
      vars.addBinding(fv._1, fld)
    }
    for (fv <- si.lookupRestrictionVars; fld <- fv._2) {
      vars.addBinding(fv._1, fld)
    }
    for (ji <- si.lookupJoinInfos.values; fv <- ji.lookupFreeVars; fld <- fv._2) {
      vars.addBinding(fv._1, fld)
    }

    val fieldsToRename = for {
      (alias, fields) <- vars
      if (si.lookupJoinInfos.contains(alias) || alias == select.relationAlias) && !joinsToMoveDown.contains(alias)
      fld <- fields
    } yield fld

    val subColumns = fieldsToRename.map(f => (f, f.name.withPrefix(f.relationAlias))).toIndexedSeq.sortBy(_._2)

    val subJoins = select.joins filter (j => !joinsToMoveDown.contains(j.relationAlias))

    val subSelect = select.copy(
      columns = subColumns,
      joins = subJoins
    )

    val renames = fieldsToRename.map(f => (f, Field(subSelectAlias, f.name.withPrefix(f.relationAlias)))).toMap
    val renameTrans = new FieldSubstitutionExprTransformer {
      override def substitute(field: Field): Expr = renames.getOrElse(field, field)
    }

    val columns = select.columns map {
      case (ex, name) => (renameTrans.transform(ex), name)
    }

    val joins = for {
      j <- select.joins
      if joinsToMoveDown.contains(j.relationAlias)
    } yield {
      (j.relation, j.joinType) match {

        case (s: SelectImpl, LeftJoinManyToOneType) =>
          // Turning left outer join to lateral form.
          // Moving join condition to where expression.
          val trans = new FieldSubstitutionExprTransformer {

            override def substitute(field: Field): Expr = if (field.relationAlias == j.relationAlias) {
              s.columns.find(_._2 == field.name) match {
                case Some((expr, _)) => expr
                case None => field
              }
            } else {
              renameTrans.transform(field)
            }
          }
          val joinCondition = trans.transform(j.condition)

          j.copy(
            joinType = LeftJoinLateralType,
            relation = s.copy(where = s.where.and(joinCondition)),
            condition = BoolLiteral(true)
          )

        case _ => j.copy(condition = renameTrans.transform(j.condition))
      }
    }

    val orderBy = select.orderBy map (ord => ord.copy(expr = renameTrans.transform(ord.expr)))

    SelectImpl(
      columns = columns,
      relation = subSelect,
      relationAlias = subSelectAlias,
      joins = joins,
      where = BoolLiteral(true),
      orderBy = orderBy
    )
  }
}
