package com.lognex.sqljedi

import com.lognex.sqljedi.Expr._
import com.lognex.sqljedi.JoinType.{InnerJoinType, LeftJoinManyToOneType, LeftJoinType}

import scala.annotation.varargs
import scala.collection.JavaConverters._

object DSL {

  // select

  def select(columns: java.util.Map[String, Expr]): SelectBuilder = {
    val b = IndexedSeq.newBuilder[(Expr, Name)]
    for ((name, col) <- columns.asScala) {
      b += ((col, Name(name)))
    }
    new SelectBuilder(b.result())
  }

  @varargs
  def union(select1: Select, select2: Select, selects: Select*): UnionBuilder = {
    new UnionBuilder((select1 +: select2 +: selects).toIndexedSeq)
  }

  def unionSort(column: Int): UnionSortClause = UnionSortClauseImpl(Left(column), directionIsAsc = true, Nulls.NullsMax)

  def unionSort(column: String): UnionSortClause = UnionSortClauseImpl(Right(Name(column)), directionIsAsc = true, Nulls.NullsMax)

  // joins

  def innerJoin(tableName: String, alias: Alias, condition: Expr): Join = join(InnerJoinType, tableName, alias, condition)

  def leftJoin(tableName: String, alias: Alias, condition: Expr): Join = join(LeftJoinType, tableName, alias, condition)

  def leftJoinManyToOne(tableName: String, alias: Alias, condition: Expr): Join = join(LeftJoinManyToOneType, tableName, alias, condition)

  private def join(joinType: JoinType, tableName: String, alias: Alias, condition: Expr) =
    JoinImpl(joinType, Table(Name(tableName)), alias, condition)

  def innerJoin(select: Select, alias: Alias, condition: Expr): Join = join(InnerJoinType, select, alias, condition)

  def leftJoin(select: Select, alias: Alias, condition: Expr): Join = join(LeftJoinType, select, alias, condition)

  def leftJoinManyToOne(select: Select, alias: Alias, condition: Expr): Join = join(LeftJoinManyToOneType, select, alias, condition)

  private def join(joinType: JoinType, select: Select, alias: Alias, condition: Expr) =
    JoinImpl(joinType, select, alias, condition)

  // references

  def newContext(): SQLContext = new SQLContext(Map.empty)

  def newContext(freeAliasNames: java.util.Map[Alias, String]): SQLContext = new SQLContext(freeAliasNames.asScala.toMap)

  def newAlias(): Alias = new Alias()

  def param(value: AnyRef): Expr = Parameter(new Param(value))

  // literals

  def nil: Expr = Null

  def str(value: String): Expr = StringLiteral(value)

  def num(value: Int): Expr = IntLiteral(value)

  def bool(value: Boolean): Expr = BoolLiteral(value)

  // functions

  def not(expr: Expr): Expr = Function("NOT", IndexedSeq(expr))

  @varargs
  def coalesce(expr: Expr, defaultFirst: Expr, defaultOther: Expr*): Expr = {
    val b = IndexedSeq.newBuilder[Expr]
    b += expr
    b += defaultFirst
    b ++= defaultOther
    Function("coalesce", b.result())
  }

  def nullIf(expr: Expr, nullValue: Expr): Expr = Function("nullIf", IndexedSeq(expr, nullValue))

  def max(expr: Expr): Expr = Function("max", IndexedSeq(expr))

  def min(expr: Expr): Expr = Function("min", IndexedSeq(expr))

  def div(expr1: Expr, expr2: Expr): Expr = Function("div", IndexedSeq(expr1, expr2))

  def round(expr: Expr): Expr = Function("round", IndexedSeq(expr))

  def sum(expr: Expr): Expr = Function("sum", IndexedSeq(expr))

  def count(expr: Expr): Expr = Function("count", IndexedSeq(expr))

  def length(expr: Expr): Expr = Function("length", IndexedSeq(expr))

  def upper(expr: Expr): Expr = Function("upper", IndexedSeq(expr))

  def lower(expr: Expr): Expr = Function("lower", IndexedSeq(expr))

  def substring(str: Expr, from: Expr, to: Expr): Expr = Function("substring", IndexedSeq(str, from, to))

  def getBit(bitmask: Expr, bit: Expr): Expr = Function("get_bit", IndexedSeq(bitmask, bit))

  def jsonAgg(value: Expr): Expr = Function("json_agg", IndexedSeq(value))

  def jsonbAgg(value: Expr): Expr = Function("jsonb_agg", IndexedSeq(value))

  def jsonObjectAgg(key: Expr, value: Expr): Expr = Function("json_object_agg", IndexedSeq(key, value))

  def jsonbObjectAgg(key: Expr, value: Expr): Expr = Function("jsonb_object_agg", IndexedSeq(key, value))

  def datePart(key: Expr, value: Expr): Expr = Function("date_part", IndexedSeq(key, value))
  
  @varargs
  def jsonBuildArray(exprs: Expr*): Expr = Function("json_build_array", exprs.toIndexedSeq)

  @varargs
  def jsonbBuildArray(exprs: Expr*): Expr = Function("jsonb_build_array", exprs.toIndexedSeq)

  @varargs
  def jsonBuildObject(exprs: Expr*): Expr = Function("json_build_object", exprs.toIndexedSeq)

  @varargs
  def jsonbBuildObject(exprs: Expr*): Expr = Function("jsonb_build_object", exprs.toIndexedSeq)

  @varargs
  def regexpReplace(source: Expr, pattern: Expr, replacement: Expr, flags: Expr*): Expr = {
    val b = IndexedSeq.newBuilder[Expr]
    b += source
    b += pattern
    b += replacement
    b ++= flags
    Function("regexp_replace", b.result())
  }

  def replace(text: Expr, from: Expr, to: Expr): Expr = Function("replace", IndexedSeq(text, from, to))

  def toTsVector(regConfig: Expr, text: Expr): Expr = Function("to_tsvector", IndexedSeq(regConfig, text))

  def toTsQuery(regConfig: Expr, text: Expr): Expr = Function("to_tsquery", IndexedSeq(regConfig, text))

  def tsRank(tsVector: Expr, tsQuery: Expr): Expr = Function("ts_rank", IndexedSeq(tsVector, tsQuery))

  def setWeight(tsVector: Expr, weight: Expr): Expr = Function("setWeight", IndexedSeq(tsVector, weight))

  @varargs
  def function(name: String, args: Expr*): Expr = Function(name, args.toIndexedSeq)

  // special

  def case_(): CaseBuilder = new CaseBuilder

  def exists(select: Select): Expr = select match {
    case s: Select => Exists(s)
  }

  def optimizerConfig(): OptimizerConfig = OptimizerConfigImpl()
}
