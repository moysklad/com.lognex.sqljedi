package com.lognex.sqljedi

import com.lognex.sqljedi.Expr.Case

import scala.collection.mutable

final class CaseBuilder {

  private val cases: mutable.Buffer[(Expr, Expr)] = mutable.Buffer.empty
  private var defaultCase: Option[Expr] = None

  def when(condition: Expr, value: Expr): CaseBuilder = {
    cases += ((condition, value))
    this
  }

  def else_(defaultValue: Expr): CaseBuilder = {
    defaultCase = Some(defaultValue)
    this
  }

  def end: Expr = Case(cases.toIndexedSeq, defaultCase)
}
