package com.lognex.sqljedi

import scala.annotation.varargs
import scala.collection.mutable

import scala.collection.JavaConverters._

final class UnionBuilder private[sqljedi] (private val selects: IndexedSeq[Select]){
  private val orderBy: mutable.Buffer[UnionSortClauseImpl] = mutable.Buffer.empty
  private var limit: Option[Int] = None
  private var offset: Option[Int] = None

  @varargs
  def orderBy(exprs: UnionSortClause*): UnionBuilder = {
    this.orderBy ++= exprs collect {
      case c: UnionSortClauseImpl => c
    }
    this
  }

  def orderBy(exprs: java.lang.Iterable[UnionSortClause]): UnionBuilder = {
    this.orderBy ++= exprs.asScala collect {
      case c: UnionSortClauseImpl => c
    }
    this
  }

  def limit(limit: Int): UnionBuilder = {
    require(this.limit.isEmpty, s"Limit is already set to ${this.limit.get}")
    this.limit = Some(limit)
    this
  }

  def offset(offset: Int): UnionBuilder = {
    require(this.offset.isEmpty, s"Offset is already set to ${this.offset.get}")
    this.offset = Some(offset)
    this
  }

  def build: Select = UnionImpl(selects, orderBy.toIndexedSeq, limit, offset)

  def buildExpr: Expr = build.asExpr
}
