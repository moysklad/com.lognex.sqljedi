package com.lognex.sqljedi.info

import com.lognex.sqljedi.Expr.Field
import com.lognex.sqljedi._
import com.lognex.sqljedi.info.VarType.{Free, Projection, Restriction}

import scala.collection.mutable

private[sqljedi] final class SelectInfo(select: SelectImpl) {

  private val joinInfos: Map[Alias, JoinInfo] = (select.joins map (j => j.relationAlias -> new JoinInfo(j, this))).toMap
  private var varsInfo: VarsInfo = _
  private var joinGraph: mutable.MultiMap[Alias, Alias] = _

  private def lookupJoinGraph: mutable.MultiMap[Alias, Alias] = {
    if (joinGraph == null) {
      joinGraph = new mutable.HashMap[Alias, mutable.Set[Alias]]() with mutable.MultiMap[Alias, Alias]

      for (j <- select.joins) { // joins are topologically sorted by construction
        val ji = joinInfos(j.relationAlias)
        for (freeVar <- ji.lookupFreeVars.keys if joinInfos.contains(freeVar)) {
          joinGraph.addBinding(freeVar, j.relationAlias)
        }
      }
    }
    joinGraph
  }

  def lookupJoinInfos: Map[Alias, JoinInfo] = joinInfos

  def joinUsages(relationAlias: Alias): Iterable[JoinInfo] = {
    require(joinInfos.contains(relationAlias))
    lookupJoinGraph.get(relationAlias) match {
      case Some(js) => js map joinInfos
      case None => Iterable.empty
    }
  }

  def lookupFreeVars: collection.Map[Alias, collection.Set[Field]] = lookupVarsInfo.lookupVars(Free)

  def lookupRestrictionVars: collection.Map[Alias, collection.Set[Field]] = lookupVarsInfo.lookupVars(Restriction)

  def lookupProjectionVars: collection.Map[Alias, collection.Set[Field]] = lookupVarsInfo.lookupVars(Projection)

  private def lookupVarsInfo: VarsInfo = {
    if (varsInfo == null) {
      varsInfo = new VarsInfo
      val bound = mutable.HashSet.empty[Alias]
      val fv = new VarsExprTraverser(Free, varsInfo)
      val rv = new VarsExprTraverser(Restriction, varsInfo)
      val pv = new VarsExprTraverser(Projection, varsInfo)

      select.relation match {
        case select: SelectImpl =>
          for (fv <- select.info.lookupFreeVars; fld <- fv._2) {
            varsInfo.add(Free, fld)
          }

        case _ =>
      }

      select.columns foreach {
        case (ex, _) =>
          fv.traverse(ex)
          pv.traverse(ex)
      }

      fv.traverse(select.where)
      rv.traverse(select.where)

      select.groupBy foreach {ex =>
        fv.traverse(ex)
        rv.traverse(ex)
      }
      select.orderBy foreach {ord =>
        fv.traverse(ord.expr)
        rv.traverse(ord.expr)
      }

      for (ji <- joinInfos.valuesIterator) {
        for (fv <- ji.lookupFreeVars; fld <- fv._2) {
          varsInfo.add(Free, fld)
        }
        bound += ji.alias
      }

      varsInfo.remove(Free, select.relationAlias)
      varsInfo.removeAll(Free, bound)
    }
    varsInfo
  }
}
