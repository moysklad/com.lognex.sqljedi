package com.lognex.sqljedi.info

import com.lognex.sqljedi.Alias
import com.lognex.sqljedi.Expr.Field
import com.lognex.sqljedi.info.VarType.{Free, Projection, Restriction}

import scala.collection.mutable

private[sqljedi] final class VarsInfo {

  private type Vars = mutable.Map[Alias, mutable.Set[Field]] with mutable.MultiMap[Alias, Field]

  private def emptyVars = new mutable.HashMap[Alias, mutable.Set[Field]]() with mutable.MultiMap[Alias, Field]

  private val vars: mutable.Map[VarType, Vars] = mutable.Map(
    Free -> emptyVars,
    Restriction -> emptyVars,
    Projection -> emptyVars
  )

  def add(varType: VarType, field: Field): Unit = {
    vars(varType).addBinding(field.relationAlias, field)
    ()
  }

  def remove(varType: VarType, name: Alias): Unit = {
    vars(varType) -= name
    ()
  }

  def removeAll(varType: VarType, names: TraversableOnce[Alias]): Unit = {
    vars(varType) --= names
    ()
  }

  def lookupVars(varType: VarType): collection.Map[Alias, collection.Set[Field]] = vars(varType)
}

private[sqljedi] sealed trait VarType

private[sqljedi] object VarType {

  // select >>z<<.name from x
  case object Free extends VarType

  // select ... from x where >>x<<.name = 'abc'
  case object Restriction extends VarType

  // select >>x<<.name from x
  case object Projection extends VarType
}