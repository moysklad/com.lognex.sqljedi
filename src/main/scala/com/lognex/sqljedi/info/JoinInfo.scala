package com.lognex.sqljedi.info

import com.lognex.sqljedi.Expr.Field
import com.lognex.sqljedi.JoinType.LeftJoinManyToOneType
import com.lognex.sqljedi._
import com.lognex.sqljedi.info.VarType.Free

private[sqljedi] final class JoinInfo(val join: JoinImpl, selectInfo: SelectInfo) {

  private var varsInfo: VarsInfo = _
  private var affectsResultSet: Option[Boolean] = None
  private var usedInProjections: Option[Boolean] = None

  def alias: Alias = join.relationAlias

  def lookupFreeVars: collection.Map[Alias, collection.Set[Field]] = {
    if (varsInfo == null) {
      varsInfo = JoinInfo.lookupFreeVars(join)
    }
    varsInfo.lookupVars(Free)
  }

  def doesAffectResultSet: Boolean = affectsResultSet match {

    case Some(affects) => affects

    case None =>
      val affects = join.joinType != LeftJoinManyToOneType ||
        selectInfo.lookupRestrictionVars.contains(alias) ||
        selectInfo.joinUsages(alias).exists(_.doesAffectResultSet)

      affectsResultSet = Some(affects)
      affects
  }

  def isUsedInProjections: Boolean = usedInProjections match {

    case Some(used) => used

    case None =>
      val used = selectInfo.lookupProjectionVars.contains(alias) ||
        selectInfo.joinUsages(alias).exists(_.isUsedInProjections)

      usedInProjections = Some(used)
      used
  }
}

private[sqljedi] object JoinInfo {

  def lookupFreeVars(join: JoinImpl): VarsInfo = {
    val varsInfo = new VarsInfo

    join.relation match {
      case select: SelectImpl =>
        for (fv <- select.info.lookupFreeVars; fld <- fv._2) {
          varsInfo.add(Free, fld)
        }

      case _ =>
    }

    new VarsExprTraverser(Free, varsInfo).traverse(join.condition)

    varsInfo.remove(Free, join.relationAlias)
    varsInfo
  }
}