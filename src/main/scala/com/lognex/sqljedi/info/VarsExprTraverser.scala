package com.lognex.sqljedi.info

import com.lognex.sqljedi.Expr.{Exists, Field, SubSelect}
import com.lognex.sqljedi._

private[sqljedi] final class VarsExprTraverser(varType: VarType, varsInfo: VarsInfo) extends ExprTraverser {

  override def traverse(expr: Expr): Unit = expr match {

    case SubSelect(select) => traverse(select)

    case Exists(select) => traverse(select)

    case fld: Field => varsInfo.add(varType, fld)

    case _ => super.traverse(expr)
  }

  private def traverse(select: Select): Unit = {
    select match {
      case selectImpl: SelectImpl =>
        for (fv <- selectImpl.info.lookupFreeVars; fld <- fv._2) {
          varsInfo.add(varType, fld)
        }
      case unionImpl: UnionImpl =>
        for (s <- unionImpl.selects) {
          traverse(s)
        }
    }
  }
}
