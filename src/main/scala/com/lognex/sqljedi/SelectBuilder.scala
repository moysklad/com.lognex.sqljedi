package com.lognex.sqljedi

import com.lognex.sqljedi.Expr.BoolLiteral
import com.lognex.sqljedi.JoinType.{InnerJoinType, LeftJoinManyToOneType, LeftJoinType}

import scala.annotation.varargs
import scala.collection.JavaConverters._
import scala.collection.mutable

final class SelectBuilder private[sqljedi] (private val columns: IndexedSeq[(Expr, Name)]) {

  private var relation: Option[Relation] = None
  private var relationAlias: Option[Alias] = None
  private val joins: mutable.Buffer[JoinImpl] = mutable.Buffer.empty
  private var where: Expr = BoolLiteral(true)
  private val groupBy: mutable.Buffer[Expr] = mutable.Buffer.empty
  private val orderBy: mutable.Buffer[SortClauseImpl] = mutable.Buffer.empty
  private var limit: Option[Int] = None
  private var offset: Option[Int] = None

  def from(tableName: String, alias: Alias): SelectBuilder = {
    this.relation = Some(Table(Name(tableName)))
    this.relationAlias = Some(alias)
    this
  }

  def from(select: Select, alias: Alias): SelectBuilder = {
    this.relation = Some(select)
    this.relationAlias = Some(alias)
    this
  }

  def innerJoin(tableName: String, alias: Alias, condition: Expr): SelectBuilder = {
    this.joins += JoinImpl(InnerJoinType, Table(Name(tableName)), alias, condition)
    this
  }

  def innerJoin(select: Select, alias: Alias, condition: Expr): SelectBuilder = {
    this.joins += JoinImpl(InnerJoinType, select, alias, condition)
    this
  }

  def leftJoin(tableName: String, alias: Alias, condition: Expr): SelectBuilder = {
    this.joins += JoinImpl(LeftJoinType, Table(Name(tableName)), alias, condition)
    this
  }

  def leftJoin(select: Select, alias: Alias, condition: Expr): SelectBuilder = {
    this.joins += JoinImpl(LeftJoinType, select, alias, condition)
    this
  }

  def leftJoinManyToOne(tableName: String, alias: Alias, condition: Expr): SelectBuilder = {
    this.joins += JoinImpl(LeftJoinManyToOneType, Table(Name(tableName)), alias, condition)
    this
  }

  def leftJoinManyToOne(select: Select, alias: Alias, condition: Expr): SelectBuilder = {
    this.joins += JoinImpl(LeftJoinManyToOneType, select, alias, condition)
    this
  }

  def join(join: Join): SelectBuilder = {
    join match {
      case ji: JoinImpl => this.joins += ji
    }
    this
  }

  def joins(joins: java.lang.Iterable[Join]): SelectBuilder = {
    for (j <- joins.asScala) {
      join(j)
    } 
    this
  }

  def where(expr: Expr): SelectBuilder = {
    this.where = expr
    this
  }

  @varargs
  def groupBy(exprs: Expr*): SelectBuilder = {
    this.groupBy ++= exprs
    this
  }

  def groupBy(exprs: java.lang.Iterable[Expr]): SelectBuilder = {
    this.groupBy ++= exprs.asScala
    this
  }

  @varargs
  def orderBy(exprs: SortClause*): SelectBuilder = {
    this.orderBy ++= exprs collect {
      case c: SortClauseImpl => c
    }
    this
  }

  def orderBy(exprs: java.lang.Iterable[SortClause]): SelectBuilder = {
    this.orderBy ++= exprs.asScala collect {
      case c: SortClauseImpl => c
    }
    this
  }

  def limit(limit: Int): SelectBuilder = {
    require(this.limit.isEmpty, s"Limit is already set to ${this.limit.get}")
    this.limit = Some(limit)
    this
  }

  def offset(offset: Int): SelectBuilder = {
    require(this.offset.isEmpty, s"Offset is already set to ${this.offset.get}")
    this.offset = Some(offset)
    this
  }

  def build: Select = (relation, relationAlias) match {

    case (Some(r), Some(ra)) => SelectImpl(
      columns = columns,
      relation = r,
      relationAlias = ra,
      joins = joins.toIndexedSeq,
      where = where,
      groupBy = groupBy.toIndexedSeq,
      orderBy = orderBy.toIndexedSeq,
      limit = limit,
      offset = offset
    )

    case _ => throw new IllegalArgumentException("Relation is not set")
  }

  def buildExpr: Expr = build.asExpr
}
