package com.lognex.sqljedi

import com.lognex.sqljedi.Expr._

private class ExprTransformer {

  def transform(expr: Expr): Expr = expr match {

    case IsNull(ex) => IsNull(transform(ex))

    case IsNotNull(ex) => IsNotNull(transform(ex))

    case And(exs) => And(exs map transform)

    case Or(exs) => Or(exs map transform)

    case In(ex, values) => In(transform(ex), values map transform)

    case Operator(literal, left, right) => Operator(literal, transform(left), transform(right))

    case Function(name, arguments) => Function(name, arguments map transform)

    case Case(cases, defaultCase) => Case(cases map {
      case (condition, value) => (transform(condition), transform(value))
    }, defaultCase map transform)

    case Cast(ex, typeLiteral) => Cast(transform(ex), typeLiteral)

    // do nothing
    case ex: SubSelect => ex
    case ex: Exists => ex
    case ex: Field => ex
    case ex: BoolLiteral => ex
    case ex: StringLiteral => ex
    case ex: IntLiteral => ex
    case Null => Null
    case ex: Parameter => ex
  }
}
