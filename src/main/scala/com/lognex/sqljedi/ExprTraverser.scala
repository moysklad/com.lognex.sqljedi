package com.lognex.sqljedi

import com.lognex.sqljedi.Expr._

private class ExprTraverser {

  def traverse(expr: Expr): Unit = expr match {

    case IsNull(ex) => traverse(ex)

    case IsNotNull(ex) => traverse(ex)

    case And(exs) => exs foreach traverse

    case Or(exs) => exs foreach traverse

    case In(ex, values) =>
      traverse(ex)
      values foreach traverse

    case Operator(literal, left, right) =>
      traverse(left)
      traverse(right)

    case Function(name, arguments) => arguments foreach traverse

    case Case(cases, defaultCase) =>
      cases foreach {
        case (condition, value) =>
          traverse(condition)
          traverse(value)
      }
      defaultCase foreach traverse

    case Cast(ex, _) => traverse(ex)

    // do nothing
    case _: SubSelect =>
    case _: Exists =>
    case _: Field =>
    case _: BoolLiteral =>
    case _: StringLiteral =>
    case _: IntLiteral =>
    case Null =>
    case _: Parameter =>
  }
}
