package com.lognex.sqljedi

import scala.collection.mutable

final class SQLContext private[sqljedi] (initialFreeAliasNames: Map[Alias, String]) {

  private val AliasPrefix = "__T"
  private val ParamPrefix = "__P"

  private val externalAliasNames = mutable.Map.empty[Alias, String] ++= initialFreeAliasNames
  private val generatedAliasNames = mutable.Map.empty[Alias, String]

  private val paramNames = mutable.Map.empty[Param, String]

  private def aliasSql(alias: Alias): String = externalAliasNames.get(alias) match {
    case Some(n) => n
    case None    => generatedAliasNames.getOrElseUpdate(alias, AliasPrefix + generatedAliasNames.size)
  }

  private def paramSql(param: Param): String = paramNames.getOrElseUpdate(param, ParamPrefix + paramNames.size)

  def addAliasName(alias: Alias, name: String): Unit = {
    externalAliasNames += (alias -> name)
    ()
  }

  def toSql(select: Select): String = select match {
    case s: SelectImpl => toSql(s, "")
    case s: UnionImpl => toSql(s, "")
  }

  def toSql(select: Select, commentTag: String): String = {
    val gen = new SQLGenerator(
      aliasToSql = (a: Alias) => aliasSql(a),
      paramToSql = (p: Param) => paramSql(p),
      prettyPrint = true
    )
    gen.write(select, commentTag)
    gen.result()
  }

  def toSql(expr: Expr): String = {
    val gen = new SQLGenerator(
      aliasToSql = (a: Alias) => aliasSql(a),
      paramToSql = (p: Param) => paramSql(p),
      prettyPrint = true
    )
    gen.write(expr)
    gen.result()
  }

  def toSql(join: Join): String = {
    val joinImpl = join match {
      case j: JoinImpl => j
    }

    val gen = new SQLGenerator(
      aliasToSql = (a: Alias) => aliasSql(a),
      paramToSql = (p: Param) => paramSql(p),
      prettyPrint = true
    )
    gen.write(joinImpl)
    gen.result()
  }

  def bindParams(callback: java.util.function.BiConsumer[String, AnyRef]): Unit = {
    for ((param, name) <- paramNames) {
      callback.accept(name, param.value)
    }
  }
}