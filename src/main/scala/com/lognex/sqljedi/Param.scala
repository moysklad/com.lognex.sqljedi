package com.lognex.sqljedi

private final class Param private[sqljedi] (val value: AnyRef) {

  override def toString: String = "P_" + Integer.toHexString(hashCode())
}
