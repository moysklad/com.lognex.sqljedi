package com.lognex.sqljedi

import com.lognex.sqljedi.Expr._

final class Alias {

  def $(fieldName: String): Expr = Field(this, Name(fieldName))

  override def toString: String = "A_" + Integer.toHexString(hashCode())
}
