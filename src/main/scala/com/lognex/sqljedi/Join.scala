package com.lognex.sqljedi

sealed trait Join {
  final def toSql(context: SQLContext): String = context.toSql(this)
}

private final case class JoinImpl(joinType: JoinType, relation: Relation, relationAlias: Alias, condition: Expr) extends Join

private sealed trait JoinType

private object JoinType {
  case object InnerJoinType extends JoinType
  case object LeftJoinType extends JoinType
  case object LeftJoinManyToOneType extends JoinType
  case object LeftJoinLateralType extends JoinType
}